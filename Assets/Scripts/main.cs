﻿using System;using System.Collections;using System.Collections.Generic;using UnityEngine;using UnityEngine.UI;using UnityEngine.SceneManagement;﻿using Random=UnityEngine.Random;
public class main:MonoBehaviour{
  public Text textOutput0,textOutput1,textOutput2,textOutput3,textOutput4,textOutput5,textOutput7,textOutput8,textOutput9;
  public Sprite spritebm0,spritebm1,spritebm2,spritebm3;
  public GameObject albattani,cometateste,fundodesenhoconstelacao;
  public int stage,score,hiscore,debug,lives;
  [HideInInspector] public int xpos0,ypos0,xoff0,yoff0;
  [HideInInspector] public int keybd0,xyspeed,xyspeed0,hlilitestarbt;
  [HideInInspector] public int counter,counterseconds,counterblackhole,countercomet,counteralbattani,counterstars,counterteleport,counterstagecompleted,counterstagedecrsec,counterdelaycongr,congflag,countersecondsdec;
  [HideInInspector] public int iforce,igravity,ipressed,igrlim;
  [HideInInspector] public int albattaniside,distancetest,distanceq,starsfound,starsamount,laststar;
  [HideInInspector] public int cometx0,comety0,cometd0,cometa0,cometv0;
  public string scenename,tempstrg1,tempstrg2,gamemode;
  public int[] starfoundarr=new int[24];
  public Sprite[] spritebma=new Sprite[32];
  public GameObject[] spritearrblackhole=new GameObject[4];
  public GameObject[] spritearrstar=new GameObject[24];
  public GameObject[] spritearrcomet=new GameObject[3];
  public string[,] starnameuc=new string[8,24]{
    {"","GAMMA DELPHINI","ALPHA DELPHINI","DELTA DELPHINI","BETA DELPHINI","EPSILON DELPHINI","","","","","","","","","","","","","","","","","",""},
    {"","POLARIS","BETA URSAE MINORIS","EPSILON URSAE MINORIS","ZETA URSAE MINORIS","KOCHAB","GAMMA URSAE MINORIS","ETA URSAE MINORIS","","","","","","","","","","","","","","","",""},
    {"","KAPPA CYGNI","IOTA CYGNI","THETA CYGNI","DELTA CYGNI","OMICRON CYGNI","DENEB","GAMMA CYGNI","ETA CYGNI","ALBIREO","NU CYGNI","EPSILON CYGNI","ZETA CYGNI","","","","","","","","","","",""},
    {"","ALKAID","MIZAR","ALIOTH","MEGREZ","PHECDA","TAIYANGSHOU","PSI URSAE MAJORIS","TANIA AUSTRALIS","TANIA BOREALIS","KAPPA URSAE MAJORIS","TALITHA","THETA URSAE MAJORIS","MERAK","DUBHE","UPSILON URSAE MAJORIS","TAU URSAE MAJORIS","MUSCIDA","ALULA BOREALIS","ALULA AUSTRALIS","","","",""},
    {"","ALMACH","OMEGA ANDROMEDAE","THETA ANDROMEDAE","NU ANDROMEDAE","MU ANDROMEDAE","MIRACH","?","PI ANDROMEDAE","DELTA ANDROMEDAE","EPSILON ANDROMEDAE","ZETA ANDROMEDAE","?","ETA ANDROMEDAE","ALPHERATZ","?","OMICRON ANDROMEDAE","IOTA ANDROMEDAE","KAPPA ANDROMEDAE","LAMBDA ANDROMEDAE","","","",""},
    {"","?","OMEGA HERCULIS","GAMMA HERCULIS","KORNEPHOROS","ZETA HERCULIS","EPSILON HERCULIS","RASALGETHI","DELTA HERCULIS","LAMBDA HERCULIS","MU HERCULIS","XI HERCULIS","OMICRON HERCULIS","THETA HERCULIS","IOTA HERCULIS","RHO HERCULIS","PI HERCULIS","ETA HERCULIS","SIGMA HERCULIS","TAU HERCULIS","PHI HERCULIS","CHI HERCULIS","",""},
    {"","OMICRON1 ORIONIS","OMICRON2 ORIONIS","PI1 ORIONIS","PI2 ORIONIS","PI3 ORIONIS","PI4 ORIONIS","PI5 ORIONIS","PI6 ORIONIS","BELLATRIX","MEISSA","CHI1 ORIONIS","CHI2 ORIONIS","NU ORIONIS","XI ORIONIS","MU ORIONIS","BETELGEUSE","SAIPH","ALNITAK","ALNILAM","MINTAKA","ETA ORIONIS","RIGEL",""},
    {"","","","","","","","","","","","","","","","","","","","","","","",""}};

  public string[] constellationname= new string[8]{
    "DELPHINUS","URSA MINOR","CYGNUS","URSA MAJOR","ANDROMEDA","HERCULES","ORION",""};

  public string[] constellationdescription=new string[8]{
    "Foi uma das 48 constelações listadas pelo astrónomo Ptolomeu, no século II, e continua entre as 88 constelações modern reconhecidas pela União Astronómica Internacional. É uma das menores constelações, em 69º lugar.",
    "É a constelação que contém a Estrela Polar, a estrela mais próxima do polo norte celeste.",
    "Possui 50 estrelas visíveis sem necessitar de equipamentos e numerosas estrelas duplas ou múltiplas e as mais brilhantes são Deneb e Albireo. As estrelas principais dessa constelação desenham na Via Láctea a figura de um cisne de asas abertas que às vezes é apelidada de Cruzeiro do Norte.",
    "É uma das grandes constelações do Almagesto de Ptolomeu, sendo mais conhecida no norte atualmente pela parte de seu pontilhado que lembra um arado.  Seu nome era Ursa Maior para os antigos gregos e os nativos da América do Norte. A mitologia grega explica a formação da Ursa Maior como um castigo de Zeus sobre Calisto.",
    "Localizada a cerca de 2,54 milhões de anos-luz de distância da Terra, na direção da constelação de Andrômeda. É a galáxia espiral mais próxima da Via Láctea e seu nome é derivado da constelação onde está situada, que, por sua vez, tem seu nome derivado da princesa mitológica Andrômeda.",
    "Era uma das 48 constelações antigas de Ptolomeu, nomeada em homenagem a Héracles, o antigo herói grego, chamado Hércules pelos romanos. As constelações vizinhas, segundo as delineações contemporâneas, são o Dragão, o Boeiro, a Coroa do Norte, a Serpente, o Serpentário, a Águia, a Flecha, a Raposa e a Lira.",
    "Localizada no equador celeste e, por este motivo, é visível em praticamente todas as regiões habitadas da Terra. A época mais favorável para sua observação se dá principalmente nas noites de verão no hemisfério sul, em dezembro e janeiro.",
    ""};

  void Start(){

    fundodesenhoconstelacao.SetActive(false);
    Scene scene=SceneManager.GetActiveScene();scenename=scene.name;
    switch (scenename){
      case "st1_Del":stage=1;textOutput4.text="DELPHINUS";break;
      case "st2_UMi":stage=2;textOutput4.text="URSA MINOR";break;
      case "st3_Cyg":stage=3;textOutput4.text="CYGNUS";break;
      case "st4_UMa":stage=4;textOutput4.text="URSA MAJOR";break;
      case "st5_And":stage=5;textOutput4.text="ANDROMEDA";break;
      case "st6_Her":stage=6;textOutput4.text="HERCULES";break;
      case "st7_Ori":stage=7;textOutput4.text="ORION";break;
      case "startup":
        stage=0;PlayerPrefs.SetInt("stage",stage);
        score=0;PlayerPrefs.SetInt("score",score);
        lives=0;PlayerPrefs.SetInt("lives",lives);
        gamemode="gameplay";PlayerPrefs.SetString("gamemode",gamemode);
        if (PlayerPrefs.HasKey("hiscore")==false){
          hiscore=0;PlayerPrefs.SetInt("hiscore",hiscore);}
        break;
      default:stage=1;break;}

    if (scenename!="startup"){
      textOutput0.text="";textOutput1.text="";textOutput2.text="";
      textOutput3.text="";textOutput5.text="";textOutput7.text="";
      textOutput8.text="";textOutput9.text="";}

    debug=0;gamemode="gameplay";
    score=PlayerPrefs.GetInt("score");hiscore=PlayerPrefs.GetInt("hiscore");
    comety0=0;cometx0=(int)Random.Range(0.0f,720.0f);

    laststar=0;counter=0;xyspeed=8;albattaniside=64;
    igrlim=20;iforce=igrlim*-1;igravity=iforce;ipressed=0;
    xpos0=400;ypos0=350;xoff0=-360;yoff0=-225;
    for (int a=0;a<24;a+=1){starfoundarr[a]=0;}
    starsfound=0;congflag=0;counterdelaycongr=0;
    switch (stage){
      case 1:starsamount=5;break;
      case 2:starsamount=7;break;
      case 3:starsamount=12;break;
      case 4:starsamount=19;break;
      case 5:starsamount=19;break;
      case 6:starsamount=21;break;
      case 7:starsamount=22;break;
      default:starsamount=5;break;}}

  void Update(){
    if (scenename=="startup"){
      if (Input.anyKeyDown){
        SceneManager.LoadScene("st1_Del");}}

    if (gamemode=="title"){}
    if (gamemode=="presentation"){}
    if (gamemode=="gameplay"){



      

      keybd0=0;
      if (Input.GetKey(KeyCode.LeftArrow)){keybd0+=1;xpos0-=xyspeed;albattaniside=-64;}
      if (Input.GetKey(KeyCode.RightArrow)){keybd0+=2;xpos0+=xyspeed;albattaniside=64;}

      ypos0+=(igravity/2);igravity+=1;
      if (igravity>igrlim){igravity=igrlim;}
      if (Input.GetKey(KeyCode.Z) && ipressed==0){igravity=iforce;ipressed=1;keybd0+=16;}
      if (!Input.GetKey(KeyCode.Z)){ipressed=0;}
      if (ypos0<0){ypos0=450;}
      if (ypos0>450){ypos0=0;}
      if (xpos0<-0){xpos0=720;}
      if (xpos0>720){xpos0=0;}

      if (comety0>450){comety0=0;cometx0=(int)Random.Range(0.0f,720.0f);}

      counterseconds=(counter/50)%1000;counterblackhole=(counter/5)%4;
      countercomet=(counter/5)%3;counteralbattani=(counter/5)%8;counterstars=(counter/10)%4;
      albattani.GetComponent<SpriteRenderer>().sprite=spritebma[8+counteralbattani];
      cometateste.GetComponent<SpriteRenderer>().sprite=spritebma[4+countercomet];
      distancetest=distance1((int)albattani.transform.position.x,(int)albattani.transform.position.y,(int)cometateste.transform.position.x,(int)cometateste.transform.position.y);        

      //finding stars
      for (int a=0;a<starsamount;a+=1){
        hlilitestarbt=0;if (laststar==a+1){hlilitestarbt=1;}
        spritearrstar[a].GetComponent<SpriteRenderer>().sprite=spritebma[16+counterstars+(4*(starfoundarr[a]))+(4*hlilitestarbt)];
        distanceq=distance1((int)albattani.transform.position.x,(int)albattani.transform.position.y,(int)spritearrstar[a].transform.position.x,(int)spritearrstar[a].transform.position.y);        
        if (distanceq<50 && starfoundarr[a]==0){starfoundarr[a]=1;starsfound+=1;laststar=a+1;score+=100;}}

      //teleporting
      for (int a=0;a<4;a+=1){
        spritearrblackhole[a].GetComponent<SpriteRenderer>().sprite=spritebma[3-counterblackhole];
        distanceq=distance1((int)albattani.transform.position.x,(int)albattani.transform.position.y,(int)spritearrblackhole[a].transform.position.x,(int)spritearrblackhole[a].transform.position.y);        
        if (distanceq<50 && counterteleport>100){
          counterteleport=0;
          xpos0=(int)spritearrblackhole[a^1].transform.position.x;xpos0-=xoff0;
          ypos0=(int)spritearrblackhole[a^1].transform.position.y;ypos0-=yoff0;}}

      // "colliding" with test-comet
      if (distance1((int)albattani.transform.position.x,(int)albattani.transform.position.y,(int)cometateste.transform.position.x,(int)cometateste.transform.position.y)<50){
        counter+=50;}

      //all the stars found
      if (starsfound>=starsamount){
        textOutput1.text="CONGRATURATIONS!";
        fundodesenhoconstelacao.SetActive(true);
        if (congflag==0){congflag=1;counterdelaycongr=0;}
        if (congflag==1 && counterdelaycongr>150){
          score+=(countersecondsdec*100);
          PlayerPrefs.SetInt("score",score);
          PlayerPrefs.SetInt("hiscore",hiscore);
          if (stage==1){PlayerPrefs.SetInt("stage",stage+1);SceneManager.LoadScene("st2_UMi");}
          if (stage==2){PlayerPrefs.SetInt("stage",stage+1);SceneManager.LoadScene("st3_Cyg");}
          if (stage==3){PlayerPrefs.SetInt("stage",stage+1);SceneManager.LoadScene("st4_UMa");}
          if (stage==4){PlayerPrefs.SetInt("stage",stage+1);SceneManager.LoadScene("st5_And");}
          if (stage==5){PlayerPrefs.SetInt("stage",stage+1);SceneManager.LoadScene("st6_Her");}
          if (stage==6){PlayerPrefs.SetInt("stage",stage+1);SceneManager.LoadScene("st7_Ori");}
          if (stage==7){PlayerPrefs.SetInt("stage",stage+1);SceneManager.LoadScene("startup");}}}
      else{
        countersecondsdec=90-counterseconds;}

      if(debug==1){
        textOutput0.text=xpos0.ToString("F0")+","+ypos0.ToString("F0")+","+keybd0.ToString("F0")+","+distancetest+","+starsfound+","+counterdelaycongr+","+stage+","+cometx0+","+comety0   ;}       

      textOutput2.text=starsfound.ToString("F0")+"/"+starsamount.ToString("F0");     
      textOutput3.text=starnameuc[stage-1,laststar];
      textOutput5.text=countersecondsdec.ToString("F0");
      if (hiscore<score){hiscore=score;}
      textOutput7.text=stage.ToString("F0");
      textOutput8.text=score.ToString("F0");
      textOutput9.text=hiscore.ToString("F0");
      albattani.transform.position=new Vector2(xpos0+xoff0,(ypos0+yoff0)*-1);
      albattani.transform.localScale=new Vector2(albattaniside,64f);
      cometateste.transform.position=new Vector2(cometx0+xoff0,(comety0+yoff0)*-1);
      counter+=1;counterteleport+=1;counterdelaycongr+=1;
      comety0+=1;}}

  public int distance1(int x1,int y1,int x2,int y2){
    double dx=(double)(x1-x2);double dy=(double)(y1-y2);
    int result=(int) Math.Sqrt((dx*dx)+(dy*dy));
    return result;
    }}

//other.gameObject.SetActive(false); - adicionar desenhos de constelação e usar isto




